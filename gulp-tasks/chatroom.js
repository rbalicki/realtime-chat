'use strict';

var gulp = require('gulp'),
    argv = require('yargs').argv,
    chalk = require('chalk'),
    Q = require('q'),
    logger = require('app/utils/logger')(__filename),
    chatroomModelPromise = require('app/models/Chatroom'),
    dbPromise = require('app/DB');;

gulp.task('chatroom-create', function (next) {
  var chatroomData = {};

  if (!argv.name) {
    logger.error('name parameter is required');
    return;
  }

  chatroomData.name = argv.name;

  if (argv.id) {
    chatroomData._id = argv.id;
  }

  Q.all([
    chatroomModelPromise(),
    dbPromise()
  ]).spread(function (Chatroom, connection) {
    Chatroom.createOrUpdate(chatroomData)
      .then(function createOrUpdateSucceeded (doc) {
        logger.info('success during chatroom-create', doc);
        connection.close();
      }).catch(function createOrUpdateFailed (err) {
        connection.close();
        logger.error('Error during chatroom-create', err);
      });
  });
});