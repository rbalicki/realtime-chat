'use strict';

var gulp = require('gulp'),
    path = require('path'),
    karma = require('karma'),
    notify = require('gulp-notify'),
    runSequence = require('run-sequence'),
    config = require('./config.json'),
    argv = require('./argv.js');

gulp.task('test', function(cb) {
  // NOTE: run them in sequence for increased readability
  // (this is not a functional requirement)
  runSequence('test-front', 'test-back', cb);
});

gulp.task('test-front', function(done) {{
  new (karma.Server)({
    configFile: path.join(process.cwd(), config.test.karmaConf),
    singleRun: true
  }, function(err) {
    if (err) {
      if (argv.notify) {
        notify.onError(function(err) {
          return 'front-end unit tests failed';
        })(err);
      }
      done(new Error(err));
    } else {
      done();
    }
  }).start();
}});

gulp.task('test-back', function(done) {{
  // not implemented yet
  done();
}});