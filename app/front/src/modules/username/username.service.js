'use strict';

angular.module('asapp.username')

  .factory('asapp.username.UsernameService', [

    '$q',
    'asapp.modal.ModalService',

    function UsernameService (

      $q,
      ModalService

    ) {

      var usernamePromise,
          currentUsername;

      return {
        getUsername: getUsername
      };

      /////////

      function getUsername (force, errorMessage) {
        var currentModal;

        if (!usernamePromise || force) {
          currentModal = ModalService.showModal('modules/username/username.modal.template.html', {
            controller: 'asapp.username.UsernameModalController',
            controllerAs: 'UsernameModalController',
            resolve: {
              errorMessage: function() {
                return errorMessage;
              }
            }
          });

          usernamePromise = currentModal.result
            .then(function usernameModalCallback (username) {
              return username;
            });
        }

        return usernamePromise;

      }

    }

  ]);