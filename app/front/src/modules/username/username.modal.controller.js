'use strict';

angular.module('asapp.username')

  .controller('asapp.username.UsernameModalController', [

    'lodash',
    '$uibModalInstance',
    'errorMessage',

    function UsernameModalController (

      _,
      $modalInstance,
      errorMessage

    ) {

      var vm = this;

      _.extend(vm, {
        currentUsername: '',
        closeModal: closeModal,
        hasErrorMessage: !!errorMessage,
        errorMessage: errorMessage
      });

      ///////////

      function closeModal () {
        $modalInstance.close(vm.currentUsername);
      }

    }

  ]);