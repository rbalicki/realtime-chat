'use strict';

angular.module('asapp.chat')

  .factory('asapp.chat.ChatService', [

    'lodash',
    '$q',
    '$location',
    '$websocket',
    '$rootScope',
    'asapp.chat.ChatConstant',
    'asapp.username.UsernameService',

    function ChatService (

      _,
      $q,
      $location,
      $websocket,
      $rootScope,
      ChatConstant,
      UsernameService

    ) {

      var messages = {},
          currentWebsocketPromise,
          websocketAvailable = false,
          chatListeners = [];

      addChatListener(function addMessageToMessagesObject (data) {
        messages[data.chatroom] = messages[data.chatroom] || [];
        messages[data.chatroom].push({
          message: data.message,
          username: data.username
        });
      });

      return {
        getMessages: getMessages,
        sendMessage: sendMessage,
        canChat: canChat,
        connect: connect,
        addChatListener: addChatListener
      };

      ////////////

      function getMessages (chatroom) {
        if (chatroom) {
          return messages[chatroom];
        } else {
          return messages;
        }
      }

      function sendMessage (chatMessage) {
        return getWebsocket().then(function(ws) {
          return ws.send(chatMessage);
        });
      }

      function connect () {
        getWebsocket({
          force: true
        }).then(initializeListener);
      }

      function initializeListener (ws) {
        ws.onMessage(function receivedMessage (messageEvent) {
          var data;

          try {
            data = JSON.parse(messageEvent.data);
            executeChatListeners(data);
          } catch (e) {
            console.log('Malformed data received', messageEvent.data);
          }
        });
        
      }

      function getWebsocket (config) {
        // get a websocket as a promise

        config = _.extend({
          nTriesLeft: ChatConstant.DEFAULT_CONNECTION_TRIES
        }, config);

        if (!currentWebsocketPromise || config.force) {
          currentWebsocketPromise = UsernameService.getUsername(config.force, config.errorMessage)
            .then(function (username) {
              var address = getChatWebsocketAddress(username),
                  socket,
                  deferred = $q.defer();

              try {
                socket = $websocket(address);

                addSocketHandlers();
              } catch(e) {
                // probably due to a malformed address
                deferred.reject(new Error('Could not connect socket to ' + address));
              }

              return deferred.promise;

              //////////////////

              function addSocketHandlers () {
                socket.onOpen(resolveSocket);
                socket.onError(chatWebsocketErrored);
                socket.onClose(chatWebsocketClosed);
              }

              function resolveSocket () {
                deferred.resolve(socket);
              }

              function chatWebsocketErrored (err) {
                // This is either due to the backend being down,
                // or the username being taken. Differentiating these
                // cases will probably require:
                // - accepting the connection on the back end
                // - sending an "actually we can't connect you"-type message
                // - then disconnecting.
                //
                // It does not appear that we can access the rejection headers
                // See http://stackoverflow.com/questions/34244767/accessing-websocket-handshake-rejection-headers
                if (config.nTriesLeft) {
                  getWebsocket({
                    nTriesLeft: config.nTriesLeft - 1,
                    force: true,
                    errorMessage: ChatConstant.CONNECTION_FAILED_MESSAGE
                  }).then(deferred.resolve);
                } else {
                  deferred.reject(new Error(err));
                }
              }

              function chatWebsocketClosed () {
                websocketAvailable = false;

                // This may happen asynchronously outside of the angular digest cycle
                // run a $digest to update the UI
                $rootScope.$digest();
              }
            });

          handleWebsocketAvailability(currentWebsocketPromise);
        }

        return currentWebsocketPromise;
      }

      function canChat () {
        return websocketAvailable;
      }

      function handleWebsocketAvailability (websocketPromise) {
        websocketAvailable = false;
        websocketPromise.then(function() {
          websocketAvailable = true;
        }, function() {
          websocketAvailable = false;
        });
      }

      function getChatWebsocketAddress (username) {
        // TODO: possible improvement - we could convert chat service to
        // be a provider, and set this in the config phase
        return 'ws://' + $location.host() + ':' + $location.port() + '/api/chat-message?username=' + username;
      }

      function executeChatListeners (data) {
        _.forEach(chatListeners, function executeChatListener (chatListener) {
          if (chatListener) {
            try {
              chatListener(data);
            } catch (e) {}
          }
        });
      }

      function addChatListener (listener) {
        var index = chatListeners.length;

        chatListeners.push(listener);

        return function removeListener () {
          chatListeners[index] = null;
        };
      }
    }

  ]);