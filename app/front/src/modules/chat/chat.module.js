'use strict';

angular.module('asapp.chat', [
  'angular-websocket',
  'asapp.username',
  'asapp.emoticon-control'
]);