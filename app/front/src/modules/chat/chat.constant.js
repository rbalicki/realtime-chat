'use strict';

angular.module('asapp.chat')

  .constant('asapp.chat.ChatConstant', {

    CONNECTION_FAILED_MESSAGE: 'Oops! We couldn\'t connect you. Maybe that user name is taken? Please try again.',
    DEFAULT_CONNECTION_TRIES: 3

  });
