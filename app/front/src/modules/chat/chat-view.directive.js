'use strict';

angular.module('asapp.chat')

  .directive('asappChatView', [

    function() {

      return {
        scope: {
          currentChatroom: '='
        },
        restrict: 'A',
        templateUrl: 'modules/chat/chat-view.template.html',
        controller: 'asapp.chat.ChatViewController',
        controllerAs: 'ChatViewController',
        bindToController: true
      };
    }

  ]).controller('asapp.chat.ChatViewController', [

    'lodash',
    '$q',
    'asapp.chat.ChatService',
    '$element',

    function (

      _,
      $q,
      ChatService,
      $element

    ) {

      var vm = this;

      _.extend(vm, {
        getMessages: getMessages,
        sendCurrentMessage: sendCurrentMessage,
        messageInTransit: false,
        currentMessage: '',
        addEmoticon: addEmoticon
      });

      /////////

      function getMessages () {
        return ChatService.getMessages(vm.currentChatroom);
      }

      function sendMessage (message) {
        if (message.length > 0) {
          return ChatService.sendMessage({
            chatroom: vm.currentChatroom,
            message: message
          });
        } else {
          return $q.when();
        }
      }

      function sendCurrentMessage () {
        return sendMessage(vm.currentMessage)
          .then(function sendMessageCallback () {
            var chatMessages = $element.find('.chat-messages');
            chatMessages[0].scrollTop = chatMessages[0].scrollHeight;

            vm.currentMessage = '';
          });
      }

      function addEmoticon (emoticon) {
        // add proper spacing around the emoticon so it looks aesthetically pleasing
        var hasTerminalSpace = vm.currentMessage[vm.currentMessage.length - 1] === ' ';

        if (!hasTerminalSpace && vm.currentMessage.length) {
          vm.currentMessage += ' ';
        }

        vm.currentMessage += String.fromCharCode(emoticon.unicode) + ' ';
        $element.find('.chat-input').focus();
      }


    }

  ]);