'use strict';

angular.module('asapp.main-view', [
  'asapp.chat',
  'asapp.chatroom'
]);