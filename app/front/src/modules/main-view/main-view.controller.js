'use strict';

angular.module('asapp.main-view')

  .controller('asapp.main-view.MainviewController', [

    'lodash',
    'asapp.chatroom.ChatroomService',
    'asapp.chat.ChatService',

    function MainviewController (

      _,
      ChatroomService,
      ChatService

    ) {

      var vm = this;

      _.extend(vm, {
        loadingChatrooms: true,
        getChatroomError: false,
        chatrooms: [],
        selectedChatroom: undefined,
        canChat: ChatService.canChat,
        connect: connect
      });

      fetchChatrooms().then(connect);

      //////////////////

      function fetchChatrooms () {
        return ChatroomService.getChatrooms().then(function getChatroomsSuccess (chatrooms) {
          vm.chatrooms = chatrooms;
          vm.selectedChatroom = (vm.chatrooms[0] || {}).name;

          if (!chatrooms.length) {
            vm.getChatroomError = true;
          }
        }).catch(function() {
          vm.getChatroomError = true;
        }).finally(function() {
          vm.loadingChatrooms = false;
        });
      }

      function connect () {
        ChatService.connect();
      }

    }

  ]);