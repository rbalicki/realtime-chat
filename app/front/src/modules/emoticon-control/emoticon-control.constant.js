'use strict';

angular.module('asapp.emoticon-control')

  .constant('asapp.emoticon-control.EmoticonControlConstant', {

    EMOTICONS: [
      {
        icon: 'fa-smile-o',
        unicode: '0xf118'
      },
      {
        icon: 'fa-frown-o',
        unicode: '0xf119'
      },
      {
        icon: 'fa-heart-o',
        unicode: '0xf08a'
      },
      {
        icon: 'fa-hourglass-2',
        unicode: '0xf252'
      },
      {
        icon: 'fa-star-o',
        unicode: '0xf006'
      },
      {
        icon: 'fa-thumbs-o-up',
        unicode: '0xf087'
      },
      {
        icon: 'fa-thumbs-o-down',
        unicode: '0xf088'
      },
      {
        icon: 'fa-bolt',
        unicode: '0xf0e7'
      },
      {
        icon: 'fa-flag-o',
        unicode: '0xf11d'
      }
    ]
  });