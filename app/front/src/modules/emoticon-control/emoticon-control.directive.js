'use strict';

angular.module('asapp.emoticon-control')
  
  .directive('asappEmoticonControl', [

    function asappEmoticonControlDirective () {

      return {
        restrict: 'A',
        templateUrl: 'modules/emoticon-control/emoticon-control.template.html',
        controller: 'asapp.emoticon-control.EmoticonControlController',
        controllerAs: 'EmoticonControlController',
        scope: {
          callback: '&'
        },
        bindToController: true
      };

    }

  ]).controller('asapp.emoticon-control.EmoticonControlController', [

    'asapp.emoticon-control.EmoticonControlConstant',

    function EmoticonControlController (

      EmoticonControlConstant

    ) {

      var vm = this;

      vm.emoticons = EmoticonControlConstant.EMOTICONS;

    }

  ]);