'use strict';

angular.module('asapp.api')

  .factory('asapp.api.ApiService', [

      '$http',
      '$cookies',
      'asapp.spinner.SpinnerService',

      function ApiService (

        $http,
        $cookies,
        SpinnerService

      ) {

        return {
          post: makeRequest.bind(undefined, 'POST'),
          get: makeRequest.bind(undefined, 'GET'),
          delete: makeRequest.bind(undefined, 'DELETE'),
          put: makeRequest.bind(undefined, 'PUT'),
          addPrefixToUrl: addPrefixToUrl
        };

        //////////

        function addPrefixToUrl (url) {
          return '/api/' + url;
        }

        function addCSRF (data) {
          data._csrf = $cookies['XSRF-TOKEN'];
        }

        function makeRequest (method, url, requestParameters, spin) {
          var request;

          requestParameters = requestParameters || {};

          requestParameters.method = method;

          if (['POST', 'PUT', 'PATCH', 'DELETE'].indexOf(method) !== -1) {
            requestParameters.data = requestParameters.data || {};
            addCSRF(requestParameters.data);
          }

          requestParameters.url = addPrefixToUrl(url);

          request = $http(requestParameters).then(function(response) {
            return response.data;
          });

          if (spin) {
            SpinnerService.spinDuringPromise(request);
          }

          return request;
        }
      }

    ]);