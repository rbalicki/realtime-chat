'use strict';

angular.module('asapp.modal')

  .factory(

    'asapp.modal.ModalService',

    [

      'lodash',
      '$rootScope',
      '$uibModal',

      function(_, $rootScope, $modal) {

        return {
          showModal: showModal
        };

        function showModal(templateUrl, options) {
          var modalScope = $rootScope.$new();

          options = _.extend({
            templateUrl: templateUrl,
            scope: modalScope,
            windowClass: 'asapp.modal',
            backdropClass: 'mb-modal-backdrop'
          }, options);

          modalScope.modalInstance = $modal.open(options);

          modalScope.closeModal = modalScope.modalInstance.close;
          modalScope.dismissModal = modalScope.modalInstance.dismiss;
          modalScope.backButtonText = options.backButtonText || 'Back';
          
          return modalScope.modalInstance;
        }

      }

    ]

  );