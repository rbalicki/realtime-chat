'use strict';

angular.module('asapp.modal', [
  'ui.bootstrap',
  'asapp.common',
  'asapp.globals'
]);