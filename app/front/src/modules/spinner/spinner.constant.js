'use strict';

angular.module('asapp.spinner')

  .constant('asapp.spinner.SpinnerConstant', {

    // in order to prevent spinners flashing in,
    // delay this number of milliseconds before loading a spinner
    delayTime: 300,
    maxSpinTime: 5000
  });