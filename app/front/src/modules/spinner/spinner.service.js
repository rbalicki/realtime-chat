'use strict';

angular.module('asapp.spinner')

  .factory('asapp.spinner.SpinnerService', [

    'lodash',
    '$q',
    '$timeout',
    'asapp.spinner.SpinnerConstant',

    function SpinnerService (

      _,
      $q,
      $timeout,
      SpinnerConstant

    ) {

      var curId = 0,
          // an array of id's which have requested a spinner
          activeSpinners = [],
          watchers = [],
          isSpinnerActive;

      addSpinnerWatcher(function (isActive) {
        isSpinnerActive = isActive;
      });

      return {
        spinDuringPromise: spinDuringPromise,
        addSpinner: addSpinner,
        addSpinnerWatcher: addSpinnerWatcher,
        shouldShowSpinner: shouldShowSpinner
      };

      ////////

      function spinDuringPromise (promise, config) {
        var spinner = addSpinner(config);

        promise.finally(spinner.clear);

        return promise;
      }

      function addSpinner (config) {
        var id = getNextId(),
            actualConfig = _.extend({}, SpinnerConstant, config),
            spinner = {
              id: id,
              endTime: Date.now() + actualConfig.maxSpinTime,
              startTime: Date.now() + actualConfig.delayTime
            };

        activeSpinners.push(spinner);

        // trigger a digest in delayTime and in maxSpinTime ms
        $timeout(angular.noop, actualConfig.delayTime + 1);
        $timeout(angular.noop, actualConfig.maxSpinTime + 1);

        if (!activeSpinners.length) {
          notifyWatchers();
        }

        return {
          clear: clearSpinner
        };

        //////

        function clearSpinner () {
          _.remove(activeSpinners, { id: id });

          if (!activeSpinners.length) {
            notifyWatchers();
          }
        }
      }

      function getNextId () {
        return curId++;
      }


      function addSpinnerWatcher (callback) {
        var id = watchers.length;

        watchers.push(callback);

        if (callback) {
          try {
            callback(!!activeSpinners.length);
          } catch (e) { }
        }

        return function clearSpinnerListener () {
          watchers[id] = null;
        };
      }

      function notifyWatchers () {
        var isActive = !!activeSpinners.length;

        _.forEach(watchers, function (watcher) {
          if (watcher) {
            try {
              watcher(isActive);
            } catch (e) {
              // someone didn't pass in a function or that function failed... oops
            }
          }
        });
      }

      function shouldShowSpinner () {
        return isSpinnerActive;
      }

    }

  ]);