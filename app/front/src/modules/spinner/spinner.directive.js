'use strict';

angular.module('asapp.spinner')

  .directive('asappSpinner', [

    'lodash',
    'asapp.spinner.SpinnerService',

    function SpinnerDirective (

      _,
      SpinnerService

    ) {

      return {
        restrict: 'A',
        templateUrl: 'modules/spinner/spinner.template.html',
        controller: controller,
        controllerAs: 'asappSpinnerController'
      };

      function controller () {
        // jshint validthis: true
        var vm = this;

        _.extend(vm, {
          shouldShowSpinner: SpinnerService.isSpinnerActive
        });
      }
    }
  ]);
