'use strict';

angular.module('asapp.spinner')

  .directive('asappSpinnerBlur', [

    'asapp.spinner.SpinnerService',

    function SpinnerBlurDirective (

      SpinnerService

    ) {

      return {
        restrict: 'A',
        link: link
      };

      function link (scope, elem, attrs) {
        SpinnerService.addSpinnerWatcher(function spinnerStateChanged (isActive) {
          if (isActive) {
            elem.addClass('spinner-blur');
          } else {
            elem.removeClass('spinner-blur');
          }
        });
      }
    }
  ]);
