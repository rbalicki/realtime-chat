'use strict';

angular.module('asapp.chatroom')

  .factory('asapp.chatroom.ChatroomService', [

    'asapp.api.ApiService',

    function ChatroomService (

      ApiService

    ) {

      return {
        getChatrooms: getChatrooms
      };

      /////////////

      function getChatrooms () {
        return ApiService.get('chatrooms');
      }

    }

  ]);