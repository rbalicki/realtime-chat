'use strict';

angular.module('asapp.chatroom')

  .directive('asappChatroomControl', [

    function() {

      return {
        scope: {
          chatrooms: '=',
          selectedChatroom: '='
        },
        restrict: 'A',
        templateUrl: 'modules/chatroom/chatroom-control.template.html',
        controller: 'asapp.chatroom.ChatroomControlController',
        controllerAs: 'ChatroomControlController',
        bindToController: true
      };
    }

  ]).controller('asapp.chatroom.ChatroomControlController', [

    'lodash',
    '$scope',
    'asapp.chat.ChatService',

    function ChatroomControlController(

      _,
      $scope,
      ChatService

    ) {

      var vm = this;

      _.extend(vm, {
        select: select
      });

      _.forEach(vm.chatrooms, function initializeChatroom (chatroom) {
        chatroom.unreadCount = 0;
      });

      ChatService.addChatListener(
        function chatListener (message) {
          var chatroom = _.find(vm.chatrooms, function (cr) {
            return cr.name === message.chatroom;
          });

          if (chatroom && chatroom.name !== vm.selectedChatroom) {
            chatroom.unreadCount++;
          }
        });

      //////////////

      function select (chatroom) {
        vm.selectedChatroom = chatroom.name;
        chatroom.unreadCount = 0;
      }

    }

  ]);