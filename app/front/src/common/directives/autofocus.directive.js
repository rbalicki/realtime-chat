'use strict';

angular.module('asapp.common')

  .directive('autoFocus', [

    function autoFocusDirective () {

      return {
        restrict: 'A',
        link: link
      };

      ///////////

      function link (scope, elem, attrs) {
        setTimeout(function() {
          elem.focus();
        }, 10);
      }

    }

  ]);