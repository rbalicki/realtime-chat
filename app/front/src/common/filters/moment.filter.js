'use strict';

angular.module('asapp.filters')

  .filter('moment', [

    'moment',

    function (

      moment

    ) {

      return function (date) {
        return moment(date);
      };

    }

  ]);