'use strict';

module.exports = createApp;

//////////

var express = require('express'),
    q = require('q'),
    WebsocketRouter = require('websocket').router;

function createApp() {
  var deferred = q.defer(),
      expressApp = express(),
      wsRouter = new WebsocketRouter(),

      // to the back-end, wsRouter and expressApp have the same purpose:
      // binding routes to functions. Passing them around in one
      // object ensures that they get the same treatment.
      appWrapper = {
        app: expressApp,
        ws: wsRouter
      };

  q.when(require('app/app/middleware')(appWrapper)).done(function() {
    q.when(require('app/app/routes')(appWrapper)).done(function() {
      deferred.resolve(appWrapper);
    });
  });

  return deferred.promise;
}