'use strict';

module.exports = bindChatMessagesRoutes;

////////////

var Q = require('q'),
    _ = require('lodash'),

    chatMessageControllerPromise = require('app/app/controllers/ChatMessageController.js'),
    apiTools = require('app/utils').apiTools;

function bindChatMessagesRoutes (appWrapper) {
  return Q.all([
    chatMessageControllerPromise()
  ]).spread(function chatMessageRoutesCallback (chatMessageController) {

    var chatMessageRoute = apiTools.makeRoute('chat-message');

    appWrapper.ws.mount(chatMessageRoute, undefined,
      chatMessageController.chatMessageConnectionAttempt);
  });
}