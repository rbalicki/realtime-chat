'use strict';

module.exports = bindChatroomRoutes;

////////////

var Q = require('q'),
    _ = require('lodash'),

    chatroomControllerPromise = require('app/app/controllers/ChatroomController.js'),
    apiTools = require('app/utils').apiTools;

function bindChatroomRoutes (appWrapper) {
  return Q.all([
    chatroomControllerPromise()
  ]).spread(function chatroomRoutesCallback (chatroomController) {
    var chatroomsRoute = apiTools.makeRoute('chatrooms');

    appWrapper.app.get(chatroomsRoute, chatroomController.getChatrooms);
  });
}