'use strict';

module.exports = loadRoutes;

////////////

var q = require('q'),
    requireDir = require('require-dir'),
    _ = require('lodash');

function loadRoutes (appWrapper) {
  var promises = [];

  _.forEach(requireDir(), function (routeLoader, fileName) {
    promises.push(q.when(routeLoader(appWrapper)));
  });

  return q.all(promises);
}