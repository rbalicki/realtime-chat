'use strict';

var _ = require('lodash'),
    Q = require('q'),

    chatroomModelPromise = require('app/models/Chatroom'),
    apiTools = require('app/utils').apiTools;

module.exports = getChatroomController;

//////////

function getChatroomController () {
  return Q.all([
    chatroomModelPromise()
  ]).spread(function chatroomController (Chatroom) {
    return {
      getChatrooms: getChatrooms.bind(undefined, Chatroom)
    };
  });
}

function getChatrooms (Chatroom, req, res, next) {

  Chatroom.getChatrooms()
    .then(function getChatroomsSucceeded (chatrooms) {
      apiTools.succeed(req, res, chatrooms);
    }, function getChatroomsFailed(err) {
      apiTools.fail(req, res, 500, new Error(err));
    });

}