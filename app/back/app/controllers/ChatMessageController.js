'use strict';

var _ = require('lodash'),
    Q = require('q'),

    chatroomModelPromise = require('app/models/Chatroom'),
    apiTools = require('app/utils').apiTools,
    logger = require('app/utils/logger')(__filename),

    chatConnections = [];

module.exports = getChatMessageController;

//////////

function getChatMessageController () {
  // TODO discuss persistence in a DB... can these be serialized?
  return Q.all([
    chatroomModelPromise()
  ]).spread(function chatMessageController (Chatroom) {
    return {
      chatMessageConnectionAttempt: chatMessageConnectionAttempt.bind(undefined, Chatroom)
    };
  });
}

function chatMessageConnectionAttempt (Chatroom, request) {
  var connection,
      index,
      username;

  try {
    // TODO find a better way to access the query parameters
    username = request.webSocketRequest.resourceURL.query.username;
  } catch (e) {
    request.reject(500, 'Error getting username');
    return;
  }

  if (!username) {
    request.reject(403, 'Username is required.');
    return;
  }

  if (usernameExists(username)) {
    // This rejection message gets sent as an "X-WebSocket-Reject-Reason" header
    request.reject(403, 'Username ' + username + ' is already connected. Pick a different one!');
    return;
  }

  connection = request.accept(request.origin);
  index = chatConnections.length;

  chatConnections.push({
    connection: connection,
    username: username
  });

  connection.on('message', function receivedMessage (message) {
    logger.debug('message', JSON.stringify(message.utf8Data));
    sendMessageToConnections(message, username);
  });

  connection.on('close', function removeConnectionFromPool () {
    chatConnections[index] = null;
  });

  broadcastHello(Chatroom, username);
}

function usernameExists (username) {
  return _.some(chatConnections, function (connection) {
    return connection && connection.username === username;
  });
}

function sendMessageToConnections (message, username) {
  var data;

  try {
    data = JSON.parse(message.utf8Data);
    data.username = username;
  } catch (e) {
    return; // fail silently if a malformed message is sent
  }
  chatConnections.forEach(function (outputConnection) {
    if (outputConnection) {
      outputConnection.connection.send(JSON.stringify(data), sendCallback);
    }
  });
}

function sendCallback (err) {
  // a typical error is 'Write after end', but catching the
  // connection.on('close') event seems to handle those.
  if (err) {
    logger.error(err);
  }
}

function broadcastHello (Chatroom, username) {
  Chatroom.getChatrooms().then(function receivedChatrooms (chatrooms) {
    if (chatrooms && chatrooms.length) {
      sendMessageToConnections(stringifyMessage({
        message: 'Hi! I\'m ' + username + ' and I just logged on.',
        chatroom: chatrooms[0].name
      }), username);
    }
  });
}

function stringifyMessage (message) {
  return {
    utf8Data: JSON.stringify(message)
  };
}