'use strict';

module.exports = createServer;

///////////

var path = require('path'),
    Q = require('q'),
    config = require('app/config.json'),
    appPromise = require('app/app'),
    http = require('http'),
    chalk = require('chalk'),
    WebSocketServer = require('websocket').server;

function createServer () {
  return Q.all([
    appPromise()
  ]).spread(function (appWrapper) {
    var app = appWrapper.app,
        server = http.createServer(app),
        port = process.env.PORT || config.server.port,
        wsServer = new WebSocketServer({
          httpServer: server,
          autoAcceptConnections: false
        });

    appWrapper.ws.attachServer(wsServer);

    server.listen(port, function serverSuccess() {
      console.info(chalk.green('Server listening on port ' + port));
    });
  });

}