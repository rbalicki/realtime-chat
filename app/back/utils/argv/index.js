'use strict';

var yargs = require('yargs'),
    path = require('path'),
    config = require('app/config.json');

module.exports = decorateArgv();

//////////

function decorateArgv () {
  return yargs.default(config.processArgv).argv;
}