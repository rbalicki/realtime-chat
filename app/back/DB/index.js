'use strict';

module.exports = initializeDB;

////////////

var Q = require('q'),
    dbDeferred,
    mongoose = require('mongoose'),
    logger = require('app/utils/logger')(__filename),
    path = require('path'),
    config = require('app/config.json'),
    chalk = require('chalk'),
    MONGO_PORT = 27017,
    uristring = process.env.MONGOLAB_URI ||
                process.env.MONGOHQ_URI ||
                'mongodb://localhost:' + MONGO_PORT + '/' + config.DB.name;



function initializeDB () {
  var connection;
  
  // mongoose uses mPromise, which does not have .catch, etc. methods.
  // To make it play nicely with Q, mongoose allows you to override
  // the promise implementation.
  mongoose.Promise = Q.Promise;

  if (!dbDeferred) {
    dbDeferred = Q.defer();

    connection = mongoose.createConnection(uristring);

    connection.Schema = mongoose.Schema;

    connection
      .on('error', function DBConnectionErrored (err) {
        dbDeferred.reject(err);
        logger.error('ERROR connecting to DB at ' + uristring);
      }).on('close', function DBConnectionClosed (err) {
        logger.info('DISCONNECTED from DB at ' + uristring);
      }).once('open', function DBConnectionOpened () {
        logger.info('CONNECTED to DB at ' + uristring);
        dbDeferred.resolve(connection);
      });
  }

  return dbDeferred.promise;
}