'use strict';

var Q = require('q'),
    _ = require('lodash'),
    dbPromise = require('app/DB'),
    logger = require('app/utils/logger')(__filename),
    apiTools = require('app/utils').apiTools;

var ChatroomSchema,
    ChatroomModel,
    chatroomModelPromise;

module.exports = getChatroomModel;

///////////

function getChatroomModel () {
  if (!chatroomModelPromise) {
    chatroomModelPromise = getChatroomModelPromise();
  }

  return chatroomModelPromise;
}

function getChatroomModelPromise () {
  return Q.all([
    dbPromise()
  ]).spread(function (connection) {
    ChatroomSchema = new connection.Schema({
      name: {
        type: String,
        required: true,
        index: { unique: true }
      }
    });

    ChatroomSchema.set('toJSON', {
      transform: function toJsonTransform (doc, ret, options) {
        delete ret._id;
        delete ret.__v;
      }
    });

    _.extend(ChatroomSchema.statics, {
      getChatrooms: getChatrooms,
      createOrUpdate: createOrUpdate
    });

    ChatroomModel = connection.model('Chatroom', ChatroomSchema);

    return ChatroomModel;

    ////////////// statics

    function getChatrooms () {
      // This is a lot of indirection for a simple "get" but in a real
      // scenario, we'd make sure we're only getting valid chatrooms
      // (e.g. restricting the return chatrooms to have clientId's matching
      // that of the requester)

      return ChatroomModel
        .find({})
        .exec();
    }

    function createOrUpdate (chatroomData) {
      var query = { _id: chatroomData._id },
          $set = { $set : { name: chatroomData.name }},
          newChatroom;

      if (!chatroomData.name) {
        return Q.reject(new Error('chatroomData.name is required'));
      }

      return ChatroomModel
        .findOne(query)
        .exec()
        .then(function findOneSucceeded (chatroom) {
          var deferred;

          if (chatroom) {
            logger.info('createOrUpdate: findOne succeeded', chatroom);
            // chatroom exists => update
            return ChatroomModel.update(query, $set).exec();

          } else {
            // chatroom does not exist => create new
            deferred = Q.defer();

            logger.info('createOrUpdate: findOne succeeded, no chatroom found');

            // with a more complicated Schema, this line would be at risk of throwing,
            // but that only results in the promise rejecting, which is expected behavior
            newChatroom = new ChatroomModel(chatroomData);

            newChatroom.save(apiTools.resolveOrReject.bind(undefined, deferred));

            return deferred.promise;
          }
        });
    }


  });
}