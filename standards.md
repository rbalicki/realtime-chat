## Front end standards

These are mostly followed:

* Regular controllers (main-view) should basically just unwrap promises and expose them.
* Directive controllers should only expose an API and expose variables. Initialization should happen in a link funciton. (This is pedantic, and doesn't matter so long as you're consistent.)
* Promise unwrapping in a controller should take the form:

        var vm = this;

        function loadChats () {

          vm.chatsLoading = true;
          vm.chatsError = false;
          vm.chats = undefined;

          ChatService.getChats().then(function getChatsSuccess (chats) {
            vm.chats = chats;
          }).catch(function getChatsError () {
            vm.chatsError = true;
          }).finally(function getChatsFinally () {
            vm.chatsLoading = false;
          });
        }

In fact, that should probably be done by a utils service, e.g.

    // one day, the above will be replaced by:
    UtilsService.unwrapPromise(vm, 'chats', ChatService.getChats());

## Javascript Standards

* Use the revealing module pattern
* Declare var's on top of the function, no exceptions
* Name anonymous functions when feasible

## Node standards

* Every core module should be require its dependencies and return a promise.
* Every core module should be require'able on it's own, and its functionality should work
