# ASAPP Javascript Full Stack Systems Architecture Challenge

Thank you for such a fun challenge! I had a blast getting this working.

In the end, this project was relatively straightforward. It fit well into the boilerplate framework I've built up over several projects. I see this demo as a culmination of the best practices I've learned over time (how to break up modules, to use pure functions whenever possible, etc.)

That isn't to say there was nothing new or improved this time around. For example, I've learned much more about websockets, and the deployment process went more smoothly because I worked off of an image of my personal website server and operate off of that, etc. I expect to take the improvements I made for this demo and fold them back into my personal website and boilerplate.

Much of the non-specific code (e.g. most of the build process, the API service, etc.) comes from other projects I've worked on. Nonetheless, all of the code was written by me at some point.

Also, I noticed that the challenge did not have follow up questions (unlike the other challenges). So, I figured I'd give a general architectural overview and a discussion of the pros and cons of my approach.

## Table of contents

* Installing dependencies and running the app locally
* Deviations from spec
* Architectural overview
    * Front-end
    * Back-end
    * Build, configuration and deployment processes
    * Image manipulation
    * Websockets
* Highlights / successes
* Lowlights / challenges
* Future TODOs

## Installing dependencies and running the app locally


* mongod is installed and running
* Node.js, npm, bower and git are installed

Then execute

    git clone https://bitbucket.org/rbalicki/realtime-chat
    cd realtime-chat
    npm install
    bower install
    npm install -g gulp

    # Repeat this at least once
    # NameOfChatroom probably has to be a single word, I haven't tested it
    gulp chatroom-create --name NameOfChatroom

    gulp

Now, you should be able to navigate to `localhost:1337` and view the app.

## Deviations from spec

* The spec is unclear about whether or not the client is supposed to display the "most recently displayed message for each room" or whether "you don't need to support full chat room history - clients only need to be notified of messages in real time as they happen." The latter seems more consistent with the overall gist, so I went with that. But note that 
* I misread the hover element section, and worked off of the idea that I'd be zooming the emoticon in the z-direction, such that it overlapped its neighbors. Happy to fix that.

## Architectural overview

### Front-end

#### Module dependencies and data

The front-end is divided into a series of modules. The dependency structure is as follows, where `=>` means "depends on":

    main-view => chatroom => api => spinner (not used in this project)
              => chat => angular-websocket
                      => username => modal => ui bootstrap modal
                      => emoticon-control

> **Note:** I forked angular-websocket solely to remove a reference to a min.js.map file. See lowlights for a discussion.

Two pieces of data are held by the main-view controller, the currently selected chatroom, and the list of chatrooms.

Everything else (the username, the messages) are stored in the username service and the chat service.

> **Note:** I think that there should be a distinction between the chat UI and chat infrastructure modules. The two are distinguished within the module, but it could be improved. The same applies to the chatroom module. See lowlights for a discussion.

#### Standards

See standards.md.

### Back-end

> **Note:** There is an alias to the `app/back` folder in `node_modules`, as `app`. Hence `require('app/utils/logger')` resolves to `require('./app/back/utils/logger')` from the project root.

#### App bootstrap

With exceptions like `app/utils/logger`, which has no dependencies, all the modules in the app return promises. This is to make it easy for modules to asynchronously require their dependencies, and only resolve once their dependencies resolve. This means that **any module is a valid entry point into the app**. For example, `gulp chatroom-create` just requires the Chatroom model (which depends on the DB connection) and uses the model directly. There is no "app loader" required.

The core app is a series of modules with the following dependency structure, where `=>` indicates "depends on":

    loader => server => app => routes => controllers => models => DB

So in practice, the app bootstraps in reverse order: the database loads, then the models load, etc.

#### Models

The only model in this is the `Chatroom` model.

The pattern used is to return the `Chatroom` model object, but to only use its explicitly defined static methods as its public API. In a future iteration, perhaps I will expose only the static methods.

Though its not always possible, all (or most) of the 'real work' of the app should be done on model objects.

#### Controllers

Controllers' roles are to unwrap calls to the Model (which return promises), and execute `apiTools.succeed` and `apiTools.fail`. (Currently, `apiTools` doesn't do much, but in a larger app, you can imagine that having a single place to implement the [JSON Api](http://jsonapi.org/) or to handle errors consistently would be important.)

In this app, `ChatMessageController` probably does too much. See lowlights for a discussion.

#### Routes

Route files associate calls to controllers with endpoints.

### Build, configuration and deployment processes

#### Build

The build process is standard gulp, by and large. Notable features:

* It is very heavily configuration-based
* `live-reload.js` includes a custom gulp plugin (the function reload), which calls out to the live reload server, updating the modified files.

#### Configuration

In `gulp-tasks/chatroom.js`, gulp directly requires the `Chatroom` model. Once that model resolves, it calls `createOrUpdate`.

In other projects I've worked on, I've had to create separate (and thus probably inconsistent) ways to access the DB and seed data.

#### Deployment

I created an image of the server that runs my personal website, saving a lot of time.

There is a tmux session that runs the server, accessible via

    npm run ssh # which ssh's into the client
    tmux attach -t server

Updating the server (this can obviously be improved) is achievable via

    npm run ssh
    cd realtime-chat
    git pull
    npm install
    bower install
    tmux attach -t server
    # ctrl-c
    # up
    # enter

### Image manipulation

The background image was taken from unsplash.com, saved as `forest.jpeg` in `app/front/common/images/` and manipulated with the following command:

`convert forest.jpeg -blur 0x3 -scale 50% -fill white -colorize 20% forest-blur.jpeg && open forest-blur.jpeg`

### Websockets

Dealing with websockets was the primary technical challenge of this app.

The implementations of websockets used an `EventEmitter` pattern (`onError()` etc.). For one-time-only events (open, close, error => close), we can return a promise that emulates this behavior.

For callbacks that are executed multiple times, e.g. `.onMessage`, we can use a pub/sub mechanism. Listeners are registered (Chat service internally registers `storeMessage` and externally exposes `addChatListener`).

Since each `onMessage` callback triggers a $digest, we can also rely on Angular's data-binding as a more convenient way to implement pub/sub (`getMessages`). Still, this feels like cheating :)

See lowlights for some drawbacks to my implementation.

## Highlights / successes

All in all, I encountered no serious barriers to finishing the project. Success!

The "Every module is a promise" pattern continues to work well, for example with the `gulp chatroom-create` requiring the chat model directly.

## Lowlights / challenges

### Known bug: you can connect twice from the client

If you click on the (inaccessible via the mouse!) "Connect to chat server" button while the modal is open (e.g. by using Vimium), you can connect to the server multiple times, resulting in your messages getting printed in your console multiple times.

The server should probably stop that, but I didn't explore this possibility.

The front-end should also stop that.

### `appWrapper`

The express app exposes get, post, put, etc. The websockets router exposes mount. These are essentially the same method, but for different implementations. A consumer of the app should not care about which implementation its getting (obviously, it knows based on whether it calls post or mount). The feel of the app could be improved if all "register a route" methods were exposed on a single object.

### Build process

* It doesn't watch new files
* There can be improved error handling with respect to less and jade files
* `build-scripts` takes too long because every dependency is re-minified. Caching the minified files should substantially speed up the process (also, that means that the files must be minified and then concatenated.)

### Separation between UI and services

The front end could be improved by separating the chat UI components from the chat services, and likewise for chatrooms. This would enable another team within the same organization to inject the core modules but to reskin the UI.

### Customized css files, etc. in /vendor

The build process and back end is opinionated about what files are exposed where. However, CSS and JS files can result in requests that do not conform to this spec (e.g. `url(somethingElse)` or with `//# sourceMappingURL=/path/to/file.js.map`). In order to remove these unsightly, failing requests, we need to manually modify the resulting files.

A solution could be to use gulp to inline resources (which will probably work, since the resources are relative to the built file) or to remove references.

### ChatMessageController Chat.Service.js do too much

I probably should have separated the files into two separate layers, each. One would deal with the websocket implementation, and another add business logic on top.

### The UI protects against very long requests, but not the back end

The back end can hang on very long chat messages. This should be addressed.

## Future TODOs

* Look into persisting to the DB the connections, so that 1. the server could scale horizontally, 2. it could potentially recover from a server restart
* Unit tests and E2E, obviously
* The client should attempt to reconnect on a dropped connection
* The spinner service exists but is not used. It creates a spinner and blurs the foreground during API requests.
* More features: It would be nice to see all of the logged in users and PM them.